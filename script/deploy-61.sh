
cd "$(dirname $0)/.." || { echo  "$LINENO cd dir error"; exit 1;}

ssh kidcoder@192.168.10.61 "rm -rf /home/kidcoder/bin/scratch-custom"
cd ..
scp -r kidcoder-editor-custom kidcoder@192.168.10.61:/home/kidcoder/bin/scratch-custom
