#!/bin/bash
cd "$(dirname $0)/.." || { echo  "$LINENO cd dir error"; exit 1;}

HOST="192.168.10.62"

git checkout dev
git pull

echo "Begin bak file..."

ssh kidcoder@${HOST} "rm -rf /home/kidcoder/bin/python-custom-bak"
ssh kidcoder@${HOST} "mv /home/kidcoder/bin/python-custom  /home/kidcoder/bin/python-custom-bak"

echo "Begin scp file..."
cd ..
scp -r kidcoder-editor-custom kidcoder@${HOST}:/home/kidcoder/bin/python-custom
