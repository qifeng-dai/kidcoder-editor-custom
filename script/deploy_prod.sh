#!/bin/bash
cd "$(dirname $0)/.." || { echo  "$LINENO cd dir error"; exit 1;}

HOST="111.1.76.77"

git checkout master
git pull

echo "Begin bak file..."

ssh kidcoder@${HOST} "rm -rf /opt/kidcoder/python-custom-bak"
ssh kidcoder@${HOST} "mv /opt/kidcoder/python-custom  /opt/kidcoder/python-custom-bak"

echo "Begin scp file..."
cd ..
scp -r kidcoder-editor-custom kidcoder@${HOST}:/opt/kidcoder/python-custom
